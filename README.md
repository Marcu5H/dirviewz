# Dirviewz

## Keybindings

| Binding | Action|
|--------------|-----------|
|`j`|Move one entry down|
|`k`|Move one entry up|
|`h`|Set the parent dir to the current dir|
|`l`|If selected entry is a dir, set that dir to the current dir|
|`:`|Command-mode (`esc` to exit)|
|`/`|Search-mode (`esc` to exit)|
|`Q`|Quit|
|`1`|Show just file names|
|`2`|Show file name, size and last modified date|
|`3`|Show file name and size|
|`4`|Show file name and last modified date|
|`o`|Open selected file|
|`P`|Enable/disable preview of selected entrie (currently only supports directories)|
|`Ctrl` + `u`|Move 10 entries up|
|`Ctrl` + `d`|Move 10 entries down|


## Missing features

+ File and directory deletions
+ Renaming dir entries
+ Moving files and directories
