/*
** DirViewz - A small terminal based file explorer
** Copyright (C) 2023  Marcu5h

** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.

** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.

** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::{
    collections::HashMap,
    env,
    // fs::{self, File},
    fs,
    io::{self, BufRead, /*BufReader*/},
    path::{Path, PathBuf},
    process::{Command, Stdio},
    time::SystemTime,
};

use crossterm::{
    event::{self, Event, KeyCode, KeyModifiers},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use ratatui::{
    backend::CrosstermBackend,
    prelude::{Alignment, Backend, Constraint, Direction, Layout},
    style::{Color, Modifier, Style},
    text::{Line, Span},
    widgets::{Block, Borders, Cell, Paragraph, Row, Table, TableState, Wrap},
    Frame, Terminal,
};
use syntect::{
    self,
    easy::HighlightFile,
    highlighting::ThemeSet,
    parsing::SyntaxSet,
};

fn bytes_to_human_readable(bytes: u64) -> String {
    if bytes == 0 {
        return "0 B".to_string();
    }

    let units = vec!["B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"];

    let exponent = std::cmp::min(bytes.ilog(2) / 1024_u32.ilog(2), 8);
    let new_bytes = bytes / 1024_u64.pow(exponent);

    return format!("{} {}", new_bytes, units[exponent as usize]);
}

#[derive(Clone)]
struct EDirEntry {
    pub file_name: String,
    pub path: PathBuf,
    pub is_dir: bool,
    pub size: String,
    // pub permissions:
    pub modified: Option<SystemTime>,
}

fn list_dir(dir: &Path) -> Result<Vec<EDirEntry>, io::Error> {
    let mut entries: Vec<EDirEntry> = Vec::new();
    for e in fs::read_dir(dir)? {
        if let Ok(e) = e {
            let mut is_dir = false;
            // Get and save the file type information
            if let Ok(file_type) = e.file_type() {
                is_dir = file_type.is_dir();
            }

            let metadata = e.metadata();

            let d_entry: EDirEntry = EDirEntry {
                file_name: e.file_name().into_string().unwrap(), // TODO: remove unwrap (if possible)
                path: e.path(),
                is_dir,
                size: if let Ok(m) = &metadata {
                    bytes_to_human_readable(m.len())
                } else {
                    "0 B".to_string()
                },
                modified: if let Ok(m) = metadata {
                    if let Ok(modified) = m.modified() {
                        Some(modified)
                    } else {
                        None
                    }
                } else {
                    None
                },
            };
            entries.push(d_entry);
        }
    }

    return Ok(entries);
}

struct FEDir {
    pub path: PathBuf,
    pub entries: Vec<EDirEntry>,
}

impl FEDir {
    pub fn new(dir: &Path) -> Result<Self, io::Error> {
        return Ok(Self {
            path: dir.to_path_buf(),
            entries: list_dir(dir)?,
        });
    }
}

fn split_string_by_query(file_name: &String, query_len: usize, at: usize) -> Vec<(bool, String)> {
    let mut substrs: Vec<(bool, String)> = Vec::new();

    if at > 0 {
        substrs.push((false, file_name[..at].to_string()));
    }
    substrs.push((true, file_name[at..at + query_len].to_string()));
    if at + query_len < file_name.len() {
        substrs.push((false, file_name[at + query_len..].to_string()));
    }

    return substrs;
}

#[derive(PartialEq, Eq)]
enum MetadataViewMode {
    All,
    File,
    FileTime,
    FileSize,
}

impl MetadataViewMode {
    pub fn show_size(&self) -> bool {
        if *self == Self::All || *self == Self::FileSize {
            return true;
        }
        return false;
    }

    pub fn show_time(&self) -> bool {
        if *self == Self::All || *self == Self::FileTime {
            return true;
        }
        return false;
    }

    pub fn to_constraints(&self) -> Vec<Constraint> {
        match *self {
            MetadataViewMode::All => {
                return vec![
                    Constraint::Length(8),
                    Constraint::Length(18),
                    Constraint::Percentage(100),
                ];
            }
            MetadataViewMode::File => {
                return vec![Constraint::Percentage(100)];
            }
            MetadataViewMode::FileSize => {
                return vec![Constraint::Length(8), Constraint::Percentage(100)];
            }
            MetadataViewMode::FileTime => {
                return vec![Constraint::Length(18), Constraint::Percentage(100)];
            }
        }
    }
}

struct FEFile<'a> {
    // pub reader: Option<BufReader<File>>,
    pub lines: Vec<Line<'a>>,
}

fn is_text_file(path: &Path) -> bool {
    let cmd_output = Command::new("file")
        .args(["-b", "-E", "-i"])
        .arg(String::from(path.to_string_lossy()))
        .output()
        .expect("Failed to execute `file` command");

    if cmd_output.stdout.starts_with(b"text/") {
        return true;
    }

    return false;
}

struct Explorer<'a> {
    pub current_dir: FEDir,
    pub dir_table_state: TableState,
    pub cmd_line_text: String,
    pub cursor_pos: (u16, u16),
    pub cursor: bool,
    pub search_query: String,
    pub preview: bool,
    pub file_cache: HashMap<String, Option<FEFile<'a>>>,
    pub metad_view_mode: MetadataViewMode,
    pub dialog: bool,
    pub dialog_msg: String,
}

impl<'a> Explorer<'a> {
    pub fn new() -> Result<Self, io::Error> {
        return Ok(Self {
            current_dir: FEDir::new(&env::current_dir()?)?,
            dir_table_state: TableState::default(),
            cmd_line_text: String::new(),
            cursor_pos: (0, 0),
            cursor: false,
            search_query: String::new(),
            // I like to enable this option by default
            preview: true,
            file_cache: HashMap::new(),
            metad_view_mode: MetadataViewMode::FileSize,
            dialog: false,
            dialog_msg: String::new(),
        });
    }

    pub fn table_next(&mut self) {
        if self.current_dir.entries.len() == 0 {
            return;
        }
        if let Some(sindex) = self.dir_table_state.selected() {
            if sindex < self.current_dir.entries.len() - 1 {
                self.dir_table_state.select(Some(sindex + 1));
            }
        }
    }

    pub fn table_prev(&mut self) {
        if self.current_dir.entries.len() == 0 {
            return;
        }
        if let Some(sindex) = self.dir_table_state.selected() {
            if sindex > 0 {
                self.dir_table_state.select(Some(sindex - 1));
            }
        }
    }

    pub fn table_set(&mut self, to: usize) {
        self.dir_table_state.select(Some(to));
    }

    pub fn selected_entry(&self) -> Option<EDirEntry> {
        if let Some(sindex) = self.dir_table_state.selected() {
            if self.current_dir.entries.len() > 0 {
                return Some(self.current_dir.entries[sindex].clone());
            }
        }
        return None;
    }

    pub fn add_selected_to_file_cache(&mut self) -> Result<(), io::Error> {
        if let Some(selected) = self.selected_entry() {
            if let Some(selected_str) = selected.path.to_str() {
                if self.file_cache.contains_key(selected_str) {
                    return Ok(());
                }

                // DIRECTORY
                if selected.is_dir {
                    let dir = FEDir::new(&selected.path)?;

                    let mut lines: Vec<Line> = Vec::new();
                    for entry in dir.entries {
                        let is_hidden = entry.file_name.starts_with('.');
                        lines.push(Line::styled(
                            entry.file_name,
                            Style::default().fg(if entry.is_dir {
                                Color::Blue
                            } else if is_hidden {
                                Color::DarkGray
                            } else {
                                Color::White
                            }),
                        ));
                    }

                    self.file_cache.insert(
                        selected_str.to_string(),
                        Some(FEFile {
                            // reader: None,
                            lines,
                        }),
                    );

                    return Ok(());
                }

                // FILE
                if is_text_file(&selected.path) {
                    let ss = SyntaxSet::load_defaults_newlines();
                    let ts = ThemeSet::load_defaults();
                    let mut highlighter = match HighlightFile::new(
                        &selected.path,
                        &ss,
                        &ts.themes["base16-eighties.dark"],
                    ) {
                        Ok(h) => h,
                        Err(_) => {
                            self.file_cache.insert(selected_str.to_string(), None);
                            return Ok(());
                        }
                    };

                    // TODO: Only highlight visible parts of the file
                    let mut lines: Vec<Line> = Vec::new();
                    for line in highlighter.reader.lines() {
                        if let Ok(line) = line {
                            match highlighter.highlight_lines.highlight_line(&line, &ss) {
                                Ok(regions) => {
                                    let mut spans: Vec<Span> = Vec::new();
                                    for &(ref style, text) in regions.iter() {
                                        spans.push(Span::styled(
                                            text.to_string().replace("\t", "    "),
                                            Style::default().fg(Color::Rgb(
                                                style.foreground.r,
                                                style.foreground.b,
                                                style.foreground.g,
                                            )),
                                        ));
                                    }

                                    lines.push(Line {
                                        spans,
                                        alignment: None,
                                    });
                                }
                                Err(_) => {}
                            }
                        }
                    }
                    self.file_cache.insert(
                        selected_str.to_string(),
                        Some(FEFile {
                            // reader: None,
                            lines,
                        }),
                    );
                    return Ok(());
                }
                self.file_cache.insert(selected_str.to_string(), None);
            }
        }
        return Ok(());
    }
}

fn ui<B: Backend>(f: &mut Frame<B>, fe: &mut Explorer) {
    let rects = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Length(1),
                Constraint::Length(f.size().height - 2),
                Constraint::Length(1),
            ]
            .as_ref(),
        )
        .split(f.size());

    let mut dir_table_rect = rects[1];
    if fe.preview {
        let middle_rects = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([Constraint::Percentage(50), Constraint::Percentage(50)].as_ref())
            .split(rects[1]);
        dir_table_rect = middle_rects[0];

        if let Some(selected_entry) = fe.selected_entry() {
            if let Some(se_str) = selected_entry.path.to_str() {
                if let Some(cached) = fe.file_cache.get(se_str) {
                    if let Some(cached) = cached {
                        let preview = Paragraph::new(cached.lines.clone())
                            .block(Block::default().borders(Borders::empty()));
                        f.render_widget(preview, middle_rects[1]);
                    }
                }
            }
        }
    }

    let current_path_line = Paragraph::new(Line::from(vec![
        Span::styled(
            format!("-> {}/", fe.current_dir.path.to_str().unwrap_or("n/a")),
            Style::default().fg(Color::LightMagenta),
        ),
        Span::styled(
            match fe.selected_entry() {
                Some(e) => e.file_name,
                None => "<na>".to_string(),
            },
            Style::default(),
        ),
    ]))
    .block(Block::default().borders(Borders::empty()))
    .alignment(Alignment::Left)
    .wrap(Wrap { trim: true });
    f.render_widget(current_path_line, rects[0]);

    let mut rows: Vec<Row> = Vec::new();
    let mut ts_set_to_first_match = false;
    let mut ts_pos = 0;
    for (index, entry) in fe.current_dir.entries.iter().enumerate() {
        let fg = if entry.is_dir {
            Color::Blue
        } else if entry.file_name.starts_with('.') {
            Color::DarkGray
        } else {
            Color::White
        };

        let mut cells: Vec<Cell> = Vec::new();

        if fe.metad_view_mode.show_size() {
            cells.push(Cell::from(entry.size.clone()));
        }

        if fe.metad_view_mode.show_time() {
            cells.push(Cell::from(if let Some(modified) = entry.modified {
                chrono::DateTime::<chrono::Utc>::from(modified)
                    .format("%d/%m/%Y %H:%M")
                    .to_string()
            } else {
                "N/a".to_string()
            }));
        }
        if fe.search_query.len() > 0 {
            if let Some(pos) = entry.file_name.find(&fe.search_query) {
                if !ts_set_to_first_match {
                    ts_pos = index;
                    ts_set_to_first_match = true;
                }

                let substrs = split_string_by_query(&entry.file_name, fe.search_query.len(), pos);
                let mut spans: Vec<Span> = Vec::new();
                for substr in substrs.clone() {
                    spans.push(Span::styled(
                        substr.1,
                        Style::default().fg(if substr.0 { Color::Red } else { fg }),
                    ));
                }
                cells.push(
                    Line {
                        spans,
                        alignment: None,
                    }
                    .into(),
                )
            } else {
                cells.push(Cell::from(entry.file_name.clone()).style(Style::default().fg(fg)))
            }
        } else {
            cells.push(Cell::from(entry.file_name.clone()).style(Style::default().fg(fg)))
        }

        rows.push(Row::new(cells));
    }

    if ts_set_to_first_match {
        fe.table_set(ts_pos);
    }

    let constraints = fe.metad_view_mode.to_constraints();
    let e_dir = Table::new(rows)
        .block(Block::default().borders(Borders::empty()))
        .highlight_style(Style::new().add_modifier(Modifier::UNDERLINED))
        .widths(&constraints);

    f.render_stateful_widget(e_dir, dir_table_rect, &mut fe.dir_table_state);

    let cmd_line = Paragraph::new(fe.cmd_line_text.clone())
        .block(Block::default().borders(Borders::empty()))
        .alignment(Alignment::Left)
        .wrap(Wrap { trim: true });
    f.render_widget(cmd_line, rects[2]);

    if fe.cursor {
        f.set_cursor(fe.cursor_pos.0, fe.cursor_pos.1);
    }
}

pub fn run_explorer() -> Result<(), io::Error> {
    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen,)?;
    let mut terminal = Terminal::new(CrosstermBackend::new(stdout))?;
    let mut in_cmd_mode = false;
    let mut in_search_mode = false;
    let mut entry_index_stack: Vec<usize> = Vec::new();

    let mut fe = Explorer::new()?;
    fe.dir_table_state.select(Some(0));

    terminal.hide_cursor()?;
    loop {
        if fe.preview {
            fe.add_selected_to_file_cache()?;
        }

        terminal.draw(|f| ui(f, &mut fe))?;

        if let Event::Key(key) = event::read()? {
            if in_cmd_mode {
                match key.code {
                    KeyCode::Char(ch) => fe.cmd_line_text.push(ch),
                    KeyCode::Enter => {
                        if fe.cmd_line_text.len() > 1 {
                            match fe.cmd_line_text.as_str() {
                                ":q" => break,
                                _ => {}
                            }
                        }
                    }
                    KeyCode::Backspace => {
                        if fe.cmd_line_text.len() > 1 {
                            fe.cmd_line_text.pop();
                        }
                    }
                    KeyCode::Esc => {
                        in_cmd_mode = false;
                        fe.cursor = false;
                        fe.cmd_line_text = "<quit>".to_string();
                        terminal.hide_cursor()?;
                    }
                    _ => {}
                }
            } else if in_search_mode {
                match key.code {
                    KeyCode::Char(ch) => {
                        fe.search_query.push(ch);
                        fe.cmd_line_text.push(ch);
                    }
                    KeyCode::Backspace => {
                        fe.search_query.pop();
                        fe.cmd_line_text.pop();
                    }
                    KeyCode::Esc => {
                        in_search_mode = false;
                        fe.cursor = false;
                        fe.cmd_line_text = "<quit>".to_string();
                        fe.search_query.clear();
                        terminal.hide_cursor()?;
                    }
                    _ => {}
                }
            } else {
                match key.code {
                    KeyCode::Char('Q') => break,
                    KeyCode::Char('j') => {
                        fe.table_next();
                    }
                    KeyCode::Char('k') => {
                        fe.table_prev();
                    }
                    KeyCode::Char('l') => {
                        match fe.dir_table_state.selected() {
                            Some(p) => {
                                if fe.current_dir.entries[p].is_dir {
                                    entry_index_stack
                                        .push(fe.dir_table_state.selected().unwrap_or(0));
                                    let selected_entry = &fe.current_dir.entries[p];
                                    let mut new_path = fe.current_dir.path.to_path_buf();
                                    new_path.push(&selected_entry.file_name);
                                    fe.current_dir = FEDir::new(&new_path)?;
                                    fe.dir_table_state.select(Some(0));
                                }
                            }
                            None => {}
                        };
                    }
                    KeyCode::Char('h') => {
                        if fe.current_dir.path.pop() {
                            fe.current_dir = FEDir::new(&fe.current_dir.path)?;
                            fe.dir_table_state
                                .select(Some(entry_index_stack.pop().unwrap_or(0)));
                        }
                    }
                    KeyCode::Char('P') => {
                        fe.preview = !fe.preview;
                    }
                    KeyCode::Char('g') => fe.dir_table_state.select(Some(0)),
                    KeyCode::Char('G') => {
                        fe.dir_table_state.select(Some(
                            (fe.current_dir.entries.len() - 1)
                                .clamp(0, fe.current_dir.entries.len() - 1),
                        ));
                    }
                    KeyCode::Char('u') => {
                        if key.modifiers == KeyModifiers::CONTROL {
                            let selc = fe.dir_table_state.selected().unwrap();
                            fe.dir_table_state.select(Some(
                                (if selc > 10 { selc - 10 } else { 0 })
                                    .clamp(0, fe.current_dir.entries.len() - 1),
                            ));
                        }
                    }
                    KeyCode::Char('d') => {
                        if key.modifiers == KeyModifiers::CONTROL {
                            fe.dir_table_state.select(Some(
                                (fe.dir_table_state.selected().unwrap() + 10)
                                    .clamp(0, fe.current_dir.entries.len() - 1),
                            ));
                        }
                    }
                    KeyCode::Char(':') => {
                        if in_cmd_mode {
                        } else {
                            in_cmd_mode = true;
                            fe.cursor = true;
                            fe.cmd_line_text = ":".to_string();
                            terminal.show_cursor()?;
                        }
                    }
                    KeyCode::Char('/') => {
                        in_search_mode = true;
                        fe.cursor = true;
                        fe.cmd_line_text = "/".to_string();
                        terminal.show_cursor()?;
                    }
                    KeyCode::Char('1') => fe.metad_view_mode = MetadataViewMode::File,
                    KeyCode::Char('2') => fe.metad_view_mode = MetadataViewMode::All,
                    KeyCode::Char('3') => fe.metad_view_mode = MetadataViewMode::FileSize,
                    KeyCode::Char('4') => fe.metad_view_mode = MetadataViewMode::FileTime,
                    KeyCode::Char('o') => {
                        if let Some(current_entry) = fe.selected_entry() {
                            if current_entry.is_dir {
                                fe.cmd_line_text =
                                    "Error: Selected is directory, Cannot open".to_string();
                            } else {
                                Command::new("xdg-open")
                                    .arg(current_entry.path)
                                    .stderr(Stdio::null())
                                    .stdin(Stdio::null())
                                    .stdout(Stdio::null())
                                    .spawn()
                                    .expect("Failed to xdg-open");
                            }
                        } else {
                            fe.cmd_line_text = "Error: Failed to open".to_string();
                        }
                    }
                    _ => {}
                }
            }

            if in_cmd_mode || in_search_mode {
                fe.cursor_pos = (fe.cmd_line_text.len() as u16, terminal.size()?.height - 1);
            }
        }
    }

    disable_raw_mode()?;
    execute!(terminal.backend_mut(), LeaveAlternateScreen,)?;
    terminal.show_cursor()?;

    return Ok(());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn split_test() {
        let s = "onetwothree".to_string();
        let at = 3;
        let q_len = 3;
        let substrs = split_string_by_query(&s, q_len, at);
        assert_eq!(
            substrs,
            vec![
                (false, "one".to_string()),
                (true, "two".to_string()),
                (false, "three".to_string())
            ]
        );
    }

    #[test]
    fn split_test_2() {
        let s = "twothree".to_string();
        let at = 0;
        let q_len = 3;
        let substrs = split_string_by_query(&s, q_len, at);
        assert_eq!(
            substrs,
            vec![(true, "two".to_string()), (false, "three".to_string())]
        );
    }

    #[test]
    fn split_test_3() {
        let s = "twothree".to_string();
        let at = 3;
        let q_len = 5;
        let substrs = split_string_by_query(&s, q_len, at);
        assert_eq!(
            substrs,
            vec![(false, "two".to_string()), (true, "three".to_string())]
        );
    }

    #[test]
    fn bytes_to_human() {
        assert_eq!(bytes_to_human_readable(0), "0 B");
    }

    #[test]
    fn bytes_to_human1() {
        assert_eq!(bytes_to_human_readable(10), "10 B");
    }

    #[test]
    fn bytes_to_human2() {
        assert_eq!(bytes_to_human_readable(1024), "1 KiB");
    }

    #[test]
    fn bytes_to_human3() {
        assert_eq!(bytes_to_human_readable(1048576), "1 MiB");
    }
}
